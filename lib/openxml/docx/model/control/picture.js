'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var picture = function (_require) {
	_inherits(picture, _require);

	function picture() {
		_classCallCheck(this, picture);

		return _possibleConstructorReturn(this, Object.getPrototypeOf(picture).apply(this, arguments));
	}

	_createClass(picture, null, [{
		key: 'type',
		get: function get() {
			return 'control.picture';
		}
	}]);

	return picture;
}(require('../control'));

exports.default = picture;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9vcGVueG1sL2RvY3gvbW9kZWwvY29udHJvbC9waWN0dXJlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0lBQXFCOzs7Ozs7Ozs7OztzQkFDSDtBQUNoQixVQUFPLGlCQUFQLENBRGdCOzs7O1FBREc7RUFBZ0IsUUFBUSxZQUFSOztrQkFBaEIiLCJmaWxlIjoicGljdHVyZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIHBpY3R1cmUgZXh0ZW5kcyByZXF1aXJlKCcuLi9jb250cm9sJyl7XG5cdHN0YXRpYyBnZXQgdHlwZSgpe1xuXHRcdHJldHVybiAnY29udHJvbC5waWN0dXJlJ1xuXHR9XG59XG4iXX0=