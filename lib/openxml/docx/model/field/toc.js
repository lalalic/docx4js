'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var toc = function (_require) {
	_inherits(toc, _require);

	function toc(instruct) {
		_classCallCheck(this, toc);

		return _possibleConstructorReturn(this, Object.getPrototypeOf(toc).apply(this, arguments));
	}

	_createClass(toc, null, [{
		key: 'type',
		get: function get() {
			return 'field.toc';
		}
	}]);

	return toc;
}(require('./field'));

exports.default = toc;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9vcGVueG1sL2RvY3gvbW9kZWwvZmllbGQvdG9jLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0lBQXFCOzs7QUFDcEIsVUFEb0IsR0FDcEIsQ0FBWSxRQUFaLEVBQXFCO3dCQURELEtBQ0M7O2dFQURELGlCQUVWLFlBRFc7RUFBckI7O2NBRG9COztzQkFLSDtBQUFDLFVBQU8sV0FBUCxDQUFEOzs7O1FBTEc7RUFBWSxRQUFRLFNBQVI7O2tCQUFaIiwiZmlsZSI6InRvYy5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIHRvYyBleHRlbmRzIHJlcXVpcmUoJy4vZmllbGQnKXtcblx0Y29uc3RydWN0b3IoaW5zdHJ1Y3Qpe1xuXHRcdHN1cGVyKC4uLmFyZ3VtZW50cylcblx0fVxuXG5cdHN0YXRpYyBnZXQgdHlwZSgpe3JldHVybiAnZmllbGQudG9jJ31cbn1cbiJdfQ==