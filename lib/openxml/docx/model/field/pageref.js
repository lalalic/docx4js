'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var pageref = function (_require) {
	_inherits(pageref, _require);

	function pageref(instruct) {
		_classCallCheck(this, pageref);

		return _possibleConstructorReturn(this, Object.getPrototypeOf(pageref).apply(this, arguments));
	}

	_createClass(pageref, null, [{
		key: 'type',
		get: function get() {
			return 'field.pageref';
		}
	}]);

	return pageref;
}(require('./field'));

exports.default = pageref;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9vcGVueG1sL2RvY3gvbW9kZWwvZmllbGQvcGFnZXJlZi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztJQUFxQjs7O0FBQ3BCLFVBRG9CLE9BQ3BCLENBQVksUUFBWixFQUFxQjt3QkFERCxTQUNDOztnRUFERCxxQkFFVixZQURXO0VBQXJCOztjQURvQjs7c0JBS0g7QUFBQyxVQUFPLGVBQVAsQ0FBRDs7OztRQUxHO0VBQWdCLFFBQVEsU0FBUjs7a0JBQWhCIiwiZmlsZSI6InBhZ2VyZWYuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyBwYWdlcmVmIGV4dGVuZHMgcmVxdWlyZSgnLi9maWVsZCcpe1xuXHRjb25zdHJ1Y3RvcihpbnN0cnVjdCl7XG5cdFx0c3VwZXIoLi4uYXJndW1lbnRzKVxuXHR9XG5cblx0c3RhdGljIGdldCB0eXBlKCl7cmV0dXJuICdmaWVsZC5wYWdlcmVmJ31cbn1cbiJdfQ==