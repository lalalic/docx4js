'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var rangeBase = function (_require) {
	_inherits(rangeBase, _require);

	function rangeBase() {
		_classCallCheck(this, rangeBase);

		return _possibleConstructorReturn(this, Object.getPrototypeOf(rangeBase).apply(this, arguments));
	}

	_createClass(rangeBase, [{
		key: 'iterate',
		value: function iterate(visitor) {}
	}, {
		key: 'first',
		value: function first() {}
	}, {
		key: 'last',
		value: function last() {}
	}], [{
		key: 'type',
		get: function get() {
			return 'range';
		}
	}]);

	return rangeBase;
}(require('../model'));

exports.default = rangeBase;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9vcGVueG1sL2RvY3gvbW9kZWwvcmFuZ2VCYXNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0lBQXFCOzs7Ozs7Ozs7OzswQkFDWixTQUFROzs7MEJBR1Q7Ozt5QkFHRDs7O3NCQUlXO0FBQUMsVUFBTyxPQUFQLENBQUQ7Ozs7UUFYRztFQUFrQixRQUFRLFVBQVI7O2tCQUFsQiIsImZpbGUiOiJyYW5nZUJhc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyByYW5nZUJhc2UgZXh0ZW5kcyByZXF1aXJlKCcuLi9tb2RlbCcpe1xuXHRpdGVyYXRlKHZpc2l0b3Ipe1xuXG5cdH1cblx0Zmlyc3QoKXtcblxuXHR9XG5cdGxhc3QoKXtcblxuXHR9XG5cblx0c3RhdGljIGdldCB0eXBlKCl7cmV0dXJuICdyYW5nZSd9XG59XG4iXX0=